# DevOps Git Cheatsheet

This is the documentation that decribes as briefly as possible, the most useful git commands, based on a `master` and `develop` branching strategy.

Ref : https://nvie.com/posts/a-successful-git-branching-model/

## Introduction

There are two main branches.

- Master : origin/master is the main branch where the source code of HEAD always reflects a production-ready state.
- Develop : origin/develop is the main branch where the source code of HEAD always reflects a state with the latest delivered development changes for the next release.

We create new features from the origin/develop branch.

## Feature Branches

May branch off from:
`develop`
Must merge back into:
`develop`
Branch naming convention:
anything except `master`, `develop`, `release-*`, or `hotfix-*`

### Creating a feature branch off develop

This will create a new branch called 'myfeature' off the 'develop' branch.

```sh
$ git checkout -b myfeature develop
Switched to a new branch "myfeature"
```

### Incorporating a finished feature into develop

Now the feature has been coded, let's put it back in.

```sh
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff myfeature
Updating ea1b82a..05e9557
(Summary of changes)
$ git branch -d myfeature
Deleted branch myfeature (was 05e9557).
$ git push origin develop
```

## Release Branches

May branch off from:
`develop`
Must merge back into:
`develop` and `master`
Branch naming convention:
`release-*`

### Creating a release branch

Release branches are created from the develop branch.  Our code is version 1.1.5.  The next release is version 1.2.  Let's make a new branch for release 1.2.

```sh
$ git checkout -b release-1.2 develop
Switched to a new branch "release-1.2"
$ ./bump-version.sh 1.2
Files modified successfully, version bumped to 1.2.
$ git commit -a -m "Bumped version number to 1.2"
[release-1.2 74d9424] Bumped version number to 1.2
1 files changed, 1 insertions(+), 1 deletions(-)
```

### Finishing a release branch

When the state of the release branch is ready to become a real release, some actions need to be carried out. First, the release branch is merged into master (since every commit on master is a new release by definition, remember). Next, that commit on master must be tagged for easy future reference to this historical version. Finally, the changes made on the release branch need to be merged back into develop, so that future releases also contain these bug fixes.

```sh
$ git checkout master
Switched to branch 'master'
$ git merge --no-ff release-1.2
Merge made by recursive.
(Summary of changes)
$ git tag -a 1.2
```

The release is now done, and tagged for future reference.  To keep the changes made in the release branch, we need to merge those back into develop, though. In Git:

```sh
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff release-1.2
Merge made by recursive.
(Summary of changes)
```

All finished, so remove the release branch since we don’t need it anymore:

```sh
$ git branch -d release-1.2
Deleted branch release-1.2 (was ff452fe).
```

## Hotfix Branches

Hotfix branches are essentially unplanned new production releases to fix critical bugs in a production version.  For this, a hotfix branch may be branched off from the corresponding tag on the master branch that marks the production version.

May branch off from:
`master`
Must merge back into:
`develop` and `master`
Branch naming convention:
`hotfix-*`

### Creating the hotfix branch

Hotfix branches are created from the master branch. For example, say version 1.2 is the current production release running live and causing troubles due to a severe bug. But changes on develop are yet unstable. We may then branch off a hotfix branch and start fixing the problem:

```sh
$ git checkout -b hotfix-1.2.1 master
Switched to a new branch "hotfix-1.2.1"
$ ./bump-version.sh 1.2.1
Files modified successfully, version bumped to 1.2.1.
$ git commit -a -m "Bumped version number to 1.2.1"
[hotfix-1.2.1 41e61bb] Bumped version number to 1.2.1
1 files changed, 1 insertions(+), 1 deletions(-)
```

Fix the bug and commit.

```sh
$ git commit -m "Fixed severe production problem"
[hotfix-1.2.1 abbe5d6] Fixed severe production problem
5 files changed, 32 insertions(+), 17 deletions(-)
```

### Finishing a hotfix branch

When finished, the bugfix needs to be merged back into master, but also needs to be merged back into develop, in order to safeguard that the bugfix is included in the next release as well. This is completely similar to how release branches are finished.

First, update master and tag the release.

```sh
$ git checkout master
Switched to branch 'master'
$ git merge --no-ff hotfix-1.2.1
Merge made by recursive.
(Summary of changes)
$ git tag -a 1.2.1
```

Next, include the bugfix in develop, too:

```sh
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff hotfix-1.2.1
Merge made by recursive.
(Summary of changes)
```

The one exception to the rule here is that, **when a release branch currently exists, the hotfix changes need to be merged into that release branch, instead of** `develop`. Back-merging the bugfix into the release branch will eventually result in the bugfix being merged into `develop` too, when the release branch is finished. (If work in `develop` immediately requires this bugfix and cannot wait for the release branch to be finished, you may safely merge the bugfix into `develop` now already as well.)
