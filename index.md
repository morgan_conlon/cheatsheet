# DevOps Cheatsheet

This is the documentation that decribes as briefly as possible, how certain processes within DevOps are done.

## Create a new user

### Introduction

User creation breaks into two areas of authority.  For user accounts in the general office world, David Quilty ( david.quilty@inivata.com, +44 (0)1223 790746) is the first choice for all matters regarding Information Technology in the office.  He can create Active Directory accounts for use for access to all the general services such as logon to Windows machines, Office365, SSO, Outlook etc.  If you need a general new user, David is the first choice.

For user accounts in the AWS and software development area, users are created as entries into a vars file in an Ansible Playbook.  This playbook is then run against certain servers which then have their `/etc/passwd` updated with the new user account information.  All access to servers running in the cloud on AWS is done in this way using the `~/.ssh/id_rsa.pub` file as the passwordless access to the server.

#### Create the user

1. Ask the user to generate some ssh keys using the command using the article here : https://phoenixnap.com/kb/generate-ssh-key-windows-10 using the OpenSSH Client.  Once complete, have them email you the `id_rsa.pub` file.
2. Create a new branch in the Ansible repo and label it according to the ticket name, e.g.

The playbook is then run against the following servers

- `bastion`
- `master`
- `ansible`

## Using Ansible

### Introduction

There are two ansible servers which are located inside the VPN-accessible network domain in the AWS cloud.  This domain is called `ini.com`.  Once connected to the VPN, you will be able to SSH directly to these servers and run playbooks.

- `ansible01.ops.ini.com` : Runs legacy playbooks.  Will be decomissioned soon.
- `ansible02.ops.ini.com` : Runs current playbooks which have been refactored and migrated from `ansible01`.

### Running a playbook

#### The request for service

The request ticket (1) to run a playbook will come in to the DevOps group via the Atlassian Devops website at [https://inivata.atlassian.net/jira/your-work](https://inivata.atlassian.net/jira/your-work).

To see these tickets, click "DevOps" under the "Recent projects" section and then look at the ticket under the "TO DO" or "IN PROGRESS" headline.  An example ticket for playbook based system deployment is 1023, here : [https://inivata.atlassian.net/browse/DEVOPS-1023](https://inivata.atlassian.net/browse/DEVOPS-1023).  You can also use the search text box and type in `1023` and it will pull up the ticket.

In these request tickets, the requestor, typically somebody from the software development group, creates a table which shows what needs to be done.

There are ususally about 8/9 items that need to be deployed.  An entry in a typical table in the ticket might look like this.

In this example, a system called Isoma needs deployment and the development group would like the instance to be called `cluster01-uat2`.

| **System** | **Instance** | **System Branch**                                  | **DevOps branch**                  | **Status** |
| ---------------- | ------------------ | -------------------------------------------------------- | ---------------------------------------- | ---------------- |
|                  |                    |                                                          |                                          |                  |
| Isoma            | cluster01-uat2     | milestone/isomapipeline-support-for-radar-launch-mar2022 | DEVOPS-1023-ansible\_core-efreet-release | deploying        |
| [Table 1]        |                    |                                                          |                                          |                  |

#### Selecting the right playbook

Now we know what the customer wants, we need to know which playbook to use on which server.  Look in the table below in the "Also known as (a.k.a)" column for Isoma (marked in _Italics_).

Looking at the line in the table, we can see that this playbook generates instances with the stub name `cluster` and the request wants an instance with the name `cluster01-uat2`, so we are in the right place.

Clearly, we need to run the `cluster-deploy.yml` playbook on `ansible01.ops.ini.com`.

**Create Instances &c.**

| BitBucket Name  | Project    | Repo             | Located and run on server | Typical location on server                  | Also known as (a.k.a)      | Playbook in Repo         | Deploys [Name stub] | Default [Name stub] | MRD Core Branch | Scheduler | Notes        |
| --------------- | ---------- | ---------------- | ------------------------- | ------------------------------------------- | -------------------------- | ------------------------ | ------------------- | ------------------- | --------------- | --------- | ------------ |
| Harvey Road     | DevOps     | ansible_core     | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core     | RaDaR Panel Design         | radar-deploy.yml         | paneldesign         | panelqc04-dev       | v1.0.4.0        | NA        | No notes     |
| Harvey Road     | DevOps     | ansible_core     | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core     | RaDaR PanelQC              | radar-deploy.yml         | panelqc             | panelqc04-dev       | v1.0.4.0        | NA        | No notes     |
| Harvey Road     | DevOps     | ansible_core     | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core     | RaDaR                      | radar-deploy.yml         | radar               | panelqc04-dev       | v1.0.4.0        | NA        | No notes     |
| _Harvey Road_ | _DevOps_ | _ansible_core_ | _ansible01.ops.ini.com_ | _/home/username/harvey_road/ansible_core_ | _Isoma_                  | _cluster-deploy.yml_   | _cluster_         | _cluster04-dev_   | _v1.19.5.0_   | _NA_    | _No notes_ |
| Harvey Road     | DevOps     | ansible_core     | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core     | Exome / RaDaR Exome / Lexi | exome-cluster-deploy.yml | cluster             | exome01-dev         | v1.0.4.0        | NA        | No notes     |
| Harvey Road     | DevOps     | ansible_core     | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core     | Legacy / not in use        | limsd-deploy.yml         | limsd-web01         | limsd-web01-dev     | master          | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Adding a new user          | bastion-deploy.yml       | bastion01           | bastion01-ops       | NA              | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Inidata                    | inidata-deploy.yml       | inidata01           | inidata01-dev       | v2.10.2.0       | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Adding a new user          | master-deploy.yml        | master01            | master01-ops        | NA              | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Repgen                     | repgen-deploy.yml        | repgens01           | repgens01-dev       | v2.10.2.0       | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Repmod                     | repmod-deploy.yml        | repmod01            | repmod01dev         | v1.4.2.1        | NA        | No notes     |
| Harvey Road     | DevOps     | ansible          | ansible02.ops.ini.com     | /home/username/harvey_road/ansible          | Ansible                    | ansible-deploy.yml       | ansible02           | ansible02-ops       | NA              | NA        | No notes     |

**Delete Instances &c.**

| BitBucket Name | Project | Repo         | Located and run on server | Typical location                        | Also known as (a.k.a) | Playbook in Repo   | Removes [Name stub] | Default [Name stub] | MRD Core Branch | Scheduler | Notes                                                            |
| -------------- | ------- | ------------ | ------------------------- | --------------------------------------- | --------------------- | ------------------ | ------------------- | ------------------- | --------------- | --------- | ---------------------------------------------------------------- |
| Harvey Road    | DevOps  | ansible_core | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core | RaDaR Panel Design    | radar-remove.yml   | paneldesign         | panelqc04-dev       | NA              | NA        | No notes                                                         |
| Harvey Road    | DevOps  | ansible_core | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core | RaDaR PanelQC         | radar-remove.yml   | panelqc             | panelqc04-dev       | NA              | NA        | No notes                                                         |
| Harvey Road    | DevOps  | ansible_core | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core | RaDaR                 | radar-remove.yml   | radar               | panelqc04-dev       | NA              | NA        | No notes                                                         |
| Harvey Road    | DevOps  | ansible_core | ansible01.ops.ini.com     | /home/username/harvey_road/ansible_core | Isoma                 | cluster-remove.yml | cluster             | cluster01-dev       | NA              | NA        | No notes                                                         |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | NA                    | bastion-deploy.yml | bastion01           | bastion01-ops       | NA              | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | Inidata               | inidata-deploy.yml | inidata01           | inidata01-dev       | v2.10.2.0       | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | NA                    | master-deploy.yml  | master01            | master01-ops        | NA              | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | Repgen                | repgen-deploy.yml  | repgens01           | repgens01-dev       | v2.10.2.0       | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | Repmod                | repmod-deploy.yml  | repmod01            | repmod01dev         | v1.4.2.1        | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |
| Harvey Road    | DevOps  | ansible      | ansible02.ops.ini.com     | /home/username/harvey_road/ansible      | Ansible               | ansible-deploy.yml | ansible02           | ansible02-ops       | NA              | NA        | No separate playbook.  Pass "-e instance_delete=true" to delete. |

[Table 2.]

#### Pre-flight checks

Before we run the playbook, we just need to check if the cluster of the same name is already on AWS and decide what to do.  Ansible will not check this, so, this task falls to you.  If you have the AWS CLI installed, you can use the command below in Windows or Linux/MacOS to find if the instance is present.
In this case, let's check to see if `cluster01-uat2` exists.  If present, then the system will reply with the Instance ID of the matching machine.

    ``    aws ec2 describe-instances --filters "Name=tag:Name,Values=cluster01-uat2" --output text --query "Reservations[*].Instances[*].InstanceId"    ``

    - If the instance exists, contact the internal customer and confirm either redeployment via playbook, or delete the instance using a "..-delete.yml" playbook.  Instances should be programmatically deleted only.
    - If the instance does not exist, move on to the next step.

#### Connect to the Ansible server

Start by connecting to the VPN `vpn001.inivata.com` using your OpenVPN client and user ID.  The user id is typically in the format `name.surname` and should have been set up for you.

With the VPN connected, now connect to the Ansible server, namely `ansible01.ops.ini.com` in this case, from the information in the table.

`$ ssh name.surname@ansible01.ops.ini.com`

#### Prepare the playbook

If the environment is not ready, create a `repos` folder and clone the `ansible_core` repo, thus.

```sh
cd
mkdir repos
cd repos
git clone git@bitbucket.org:harveyroad/ansible_core.git
```

Now, looking at the request table, we see that we need to use the `DEVOPS-1023-ansible\_core-efreet-release` branch, so let's `fetch` the latest metadata updates, switch to that branch and pull the latest changes..

```sh
git fetch
git checkout DEVOPS-1023-ansible\_core-efreet-release
git pull
git status
```

Now we have the latest playbook and are ready to start the deployment.

#### Run the deployment

Now all we need to do is run the playbook, but first start `screen`, just in case you get disconnected.  That said, Ansible will always log your plays and outputs to `~/ansible.log` on `ansible01`.

```sh
screen
ansible-playbook cluster-deploy.yml
```

The playbook will ask for inputs.  Our inputs for the ticket are shown..

```sh
cluster name in format [[ cluster<<NUMBER>> ]] : cluster04
master server ENVIRONMENT (dev, prod, test, etc.) : dev
pipeline_core branch for installing cluster_software : milestone/isomapipeline-support-for-radar-launch-mar2022
The minimum number of instances the Auto Scaling group should have at any time (1-10) : 1
```

The playbook never asks for confirmation before starting so as soon as the last question is aksed, the process starts immediately.

N.B. : When the cluster builds, it pulls down software from the software development repos and builds and installs that.  This is what the **System Branch** entry `pipeline_core branch for installing cluster_software` in the table refers to.
When the cluster is built using the DevOps playbook, that branch of the repo will be cloned to the new cluster and built on the cluster, so there are two parts : 1. Create the instance 2. Clone the development pipeline software and build the pipeline.

## Author

```sh
+-----------------------------------------------------------------+
| t  : +44 (0)1225 685499                                         |
| e  : morgan.conlon@inivata.com                                  |
| w  : www.inivata.com                                            |
| s  : https://inivata.app.box.com/folder/156828409330/index.md   |
+-----------------------------------------------------------------+
```

For any changes to this document, please email me. All comments and feedback welcome.

---

1. (or 'story' in scrum-speak)
